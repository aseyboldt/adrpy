# Advection-Dispersion-Reaction models in Python
This package allows to couple ODE reaction models set up with [sunode](https://github.com/aseyboldt/sunode) to 1-D advective-dispersive transport, applying a finite volume discretization scheme.

## Installation
You can install the package with pip:
```
pip install git+https://gitlab.com/astoeriko/adrpy.git
```
If you use conda, you might want to install dependencies with conda first.
The required packages are listed in the [setup.py](setup.py), but it should be sufficient to [install *sunode*](https://github.com/aseyboldt/sunode#installation).

## Documentation
Some usage examples can be found in the notebooks folder. They are saved as markdown files using [jupytext](https://jupytext.readthedocs.io). To run them as jupyter notebooks, you need to pair them with an ipynb file.

Documentation on the theory can be found in the [theory guide](docs/theory_guide.md).
It contains information on the used equations how they are discretized with the finite volume method.

## Usage Example
A simple transport problem for a conservative and a reactive tracer with a constant velocity and constant boundary concentrations can be set up as follows.

First, we import the necessary libraries:
```python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sunode
import xarray as xr

import adrpy
import adrpy.velocity_funcs as velo
```

Now, we create a function that define the reactions.

```python
def reaction(t, y, p):
    return {
        "y": -p.a * y.y,  # first order decay
        "z": 0,  # conservative tracer
    }
```

Next, we need to create dictionaries that contain the parameter values for the transport and the reaction problem.
The `"c_norm"` and `"spatial_coordinate"` parameters are required, even if they are not used.
`"c_norm"` is a scaling constant for the state variables (see the theory guide) and should be set to one if it is not used.
The value of `"spatial_coordinate"` is not used as it will be overwritten with the values of the x-coordinate.
This parameter can be used to define spatially variable parameters.

```python
# Specify the dimensions of state variables and parameters
state_shapes = {"y": (), "z": ()}
transport_param_dims = {
    "dx": (), # length of each cell
    "length": (), # length of the total domain
    "transported": {"y": (), "z": ()}, # indicate if the state is subject to transport
    "v": (), # constant veloctiy
    "dispersivity": (),
    "molecular_diffusion": (),
    "porosity": (),
    "left": {"inflow_concentration": {"y": (), "z": ()}}, # left inflow concentrations
    "right": {"inflow_concentration": {"y": (), "z": ()}},
}
reaction_param_dims = {"a": (), "c_norm": {"y": (), "z": ()}, "spatial_coordinate": ()}
```

```python
# Set the parameter values
transport_param_values = {
    "dx": 0.01, # length of each cell
    "length": 2.0, # length of the total domain
    "transported": {"y": 1.0, "z": 1.0}, # indicate if the state is subject to transport
    "v": 3e-5, # constant veloctiy
    "dispersivity": 1e-2,
    "molecular_diffusion": 1e-9,
    "porosity": 0.3,
    "left": {"inflow_concentration": {"y": 1.0, "z": 1.0}}, # left inflow concentrations
    "right": {"inflow_concentration": {"y": 0.0, "z": 0.0}},
}
reaction_param_values = {
    "a": 1e-5, # decay constant
    # "c_norm" and "spatial_coordinate" are required, even if they are not used
    "c_norm": {"y": 1.0, "z": 1.0},
    "spatial_coordinate": -333.0,
}
```

Define the values of the x-coordinate:

```python
coords = {
    "x": np.arange(
        transport_param_values["dx"] / 2,
        transport_param_values["length"],
        transport_param_values["dx"],
    )
}
```

Now, we need to set up the reaction problem with *sunode*.

```python
# Set up the reaction problem
reaction_problem = sunode.SympyProblem(
    params=reaction_param_dims,
    states=state_shapes,
    rhs_sympy=reaction,
    derivative_params=(),
)
```

Now, we generate the transport problem, passing options for the boundary conditions (make sure that the parameter definitions are consistent with the boundary condition type) and the reaction problem. You can define your own function for the advective velocity. Several example functions are available from the `adrpy.velocity` module.

```python
# Set up the transport problem
transport_problem = adrpy.TransportProblem(
    reaction_problem=reaction_problem,
    left_boundary="constant_concentration",
    right_boundary="constant_concentration",
    velocity_func=velo.constant_velocity,
    transport_param_dims=transport_param_dims,
    coords=coords,
)
```

Make a solver object to solve the ODEs using *sunode*.

```python
# Generate the solver
solver = sunode.solver.Solver(
    transport_problem,
    solver="BDF",
    linear_solver="dense_finitediff",
    constraints=np.ones(transport_problem.n_states),
    abstol=1e-12,
    reltol=1e-8,
)
```

Now, we define initial conditions. They need to be given as a numpy array with the correct dtype.

```python
# Define initial conditions
y0 = np.zeros((), dtype=transport_problem.state_dtype)
y0["y"][:] = 0.0
y0["z"][:] = 0.0
t0 = 0
tvals = np.arange(0, 100000, 100)
```

Finally we can pass the actual parameter values to the solver, initialize output arrays and call the solver.

```python
# Pass parameter values to the solver
solver.set_params_dict(
    {
        "transport": transport_param_values,
        "reaction": reaction_param_values,
    }
)

# Create arrays that the solver can write to
output = solver.make_output_buffers(tvals)
user_data = transport_problem.make_user_data()

# Call the solver
solver.solve(t0=t0, tvals=tvals, y0=y0, y_out=output)
```

For convenience, we can automatically convert the solution to a xarray Dataset with named dimensions and coordinates.

```python
# Convert outputs to xarray dataset
ds = solver.as_xarray(tvals, output)
```
