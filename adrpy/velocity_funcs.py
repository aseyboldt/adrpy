"""
This module contains functions that can be used as the velocity_func argument to a
TransportProblem (these functions' names end in `_velocity`). It also contains some
functions necessary to define the velocity functions.
"""

import numba
import numpy as np


@numba.njit
def sine_wave(t, A, f, phi, m):
    """Sinusoidal function.

    Parameters
    ==========
    t: float
        time
    A: float or numpy array
        amplitude
    f: float or numpy array
        frequency
    phi: float or numpy array
        phase shift in radians
    m: float or numpy array
        mean value of the curve
    """
    return A * np.sin(2 * np.pi * f * t + phi) + m


@numba.njit
def logistic(x, x0, k, L):
    """Compute the logistic function.

    Parameters
    ----------
    x: ndarray
        Input array.
    x0: float
        The x-value of the sigmoid's midpoint.
    k: float
        The logistic growth rate or steepness of the curve.
    L: float
        The curve's maximum value.

    Returns
    -------
    y: ndarray or scalar
        The corresponding values of the logistic function.
    """
    return L / (1 + np.exp(-k * (x - x0)))


@numba.njit
def smooth_step(x, x0, k, y_minus, y_plus):
    """Compute a smooth step function based on a logistic function.

    Parameters
    ----------
    x: ndarray
        Input array.
    x0: float
        The midpoint of the step.
    k: float
        Parameter determining the steepness of the curve (larger k means a steeper
        curve).
    y_minus: float
        The y-value at the limit of x → -∞
    y_plus: float
        The y-value at the limit of x → +∞

    Returns
    -------
    y: ndarray or scalar
        The corresponding values of the smooth-step function.
    """
    return logistic(x, x0=x0, k=k, L=y_plus - y_minus) + y_minus


@numba.njit
def constant_velocity(t, params):
    """Constant velocity function.

    Parameters
    ----------
    t: ndarray or float
        Time points at which the velocity will be evaluated.
    params
        Transport parameters. They must contain the key "v" for the constant velocity.
    """
    return params.v


@numba.njit
def sinusoidal_velocity(t, params):
    """Sinusoidal velocity function.

    Parameters
    ----------
    t: ndarray or float
        Time points at which the velocity will be evaluated.
    params
        Transport parameters. They must contain the following (nested) keys:
        ("v", "amplitude"): float
            Velocity amplitude.
        ("v", "frquency"): float
            Frequency of velocity fluctuations.
        ("v", "phase_shift"): float
            Phase shift in radians.
        ("v", "mean_val"): float
            Mean velocity.
    """
    return sine_wave(
        t,
        params.v.amplitude,
        params.v.frequency,
        params.v.phase_shift,
        params.v.mean_val,
    )


@numba.njit
def interpolate_velocity(t, params):
    """Velocity function that linearly interpolates between velocity values.

    Parameters
    ----------
    t: ndarray or float
        Time points at which the velocity will be evaluated.
    params
        Transport parameters. They must contain the following (nested) keys:
        ("v", "t_values"): ndarray
            Time points at which the velocity is given.
        ("v", "v_values"): ndarray
            Velocity values to use for the interpolation.
    """
    return np.interp(t, params.v.t_values, params.v.v_values)


@numba.njit
def smooth_step_velocity(t, params):
    """Smoothed-step velocity function.

    Parameters
    ----------
    t: ndarray or float
        Time points at which the velocity will be evaluated.
    params
        Transport parameters. They must contain the following (nested) keys:
        ("v", "t_step"): float
            Midpoint time of the step.
        ("v", "steepness"): ndarray
            Steepness parameter of a logistic function. It must be
            larger than zero, and larger values make the step steeper.
        ("v", "before_step"): float
            Constant velocity before the step.
        ("v", "after_step"): float
            Constant velocity after the step.
    """
    return smooth_step(
        t,
        params.v.t_step,
        params.v.steepness,
        params.v.before_step,
        params.v.after_step,
    )
