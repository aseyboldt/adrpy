---
title: 'Theory guide for the *adrpy* module'
author: Anna Störiko
...

## 1-D Advection-Dispersion-Reaction equation
*adrpy* solves the 1-dimensional advection-dispersion-reaction equation with uniform coefficients:
```math
\frac{\partial c_j}{\partial t} + v \frac{\partial c_j}{\partial x} - D \frac{\partial^2 c_j}{\partial x^2} = r_{\text{net}}^j\,,
\tag{1}
```
where $`v`$ is the advection velocity, $`D`$ is the dispersion coefficient and $`c_j`$ and $`r_{\text{net}}^j`$ are the concentration and net reaction rate of compund $`j`$.
The advection velocity relates to the specific discharge $`q`$ by the porosity $`\theta`$, that is, we assume a constant porosity.
Dispersion is parameterized following Scheidegger (1974):
```math
D = \lvert v\rvert \alpha_L + D_e\,,
```
where $`\alpha_L`$ is the longitudinal dispersivity and $`D_e`$ denotes the pore-diffusion coefficient.

## Discretization with the Finite Volume Method
We discretize equation (1) using the cell-centered finite volume method (FVM). To this end, the transport equation for species $`j`$ integrated over the control volume $`i`$ with volume $`V = \Delta x A`$ (Note: we assume here that all cells have the same geometry, i.e. $`A`$ and $`\Delta x`$ are constant):
```math
V \theta \frac{\partial \tilde{c}_i^j}{\partial t} + \int_{\Gamma_i} \mathbf{n} \cdot \mathbf{J}_j\, d\Gamma = V \theta \tilde{r}_i^j\,,
\tag{2}
```
where $`\Gamma_i`$ is the total boundary of control volume $`i`$, $`\mathbf{n}`$ is the normal vector and $`\mathbf{J_j}`$ are the fluxes of species $`j`$ over the boundaries. The tilde denotes that we now consider averages over the control volume as an approximation of the primary variable.

In 1-D, the integral of the fluxes can be expressed as the sum of the fluxes crossing the two boundaries:
```math
\int_{\Gamma_i} \mathbf{n} \cdot \mathbf{J}_j\, d\Gamma = A (J_{i + 1/2}^j - J_{i - 1/2}^j)
```
The total flux is the sum of the advective and the dispersive flux.

### Advective fluxes
The advective fluxes at the interfaces (with constant specific discharge) are given by:
```math
\begin{gathered}
J_{i + 1/2}^{j,\text{adv}} = q \tilde{c}_{i + 1/2}^j\\
J_{i - 1/2}^{j,\text{adv}} = q \tilde{c}_{i - 1/2}^j
\end{gathered}
```
Using upstream weighting for the concentrations at the boundaries we obtain:
```math
\begin{gathered}
J_{i + 1/2}^{j,\text{adv}} = q \tilde{c}_{i}^j\\
J_{i - 1/2}^{j,\text{adv}} = q \tilde{c}_{i - 1}^j
\end{gathered}
```
if $`q`$ is positive and
```math
\begin{gathered}
J_{i + 1/2}^{j,\text{adv}} = q \tilde{c}_{i+1}^j\\
J_{i - 1/2}^{j,\text{adv}} = q \tilde{c}_{i}^j
\end{gathered}
```
for negative $`q`$.

### Dispersive fluxes
The dispersive fluxes at the interfaces (with uniform dispersion coefficient) are:
```math
\begin{gathered}
J_{i + 1/2}^{j,\text{dis}} = - \theta D_j \left. \frac{\partial c}{\partial x}\right|_{i + 1/2}\\
J_{i - 1/2}^{j,\text{dis}} = - \theta D_j \left. \frac{\partial c}{\partial x}\right|_{i - 1/2}
\end{gathered}
```
Approximating the gradients at the boundary by finite differences yields:
```math
\begin{gathered}
J_{i + 1/2}^{j,\text{dis}} = - \theta D_j \frac{\tilde{c}_{i+1}^j - \tilde{c}_i^j}{\Delta x}\\
J_{i - 1/2}^{j,\text{dis}} = - \theta D_j \frac{\tilde{c}_i^j - \tilde{c}_{i-1}^j}{\Delta x}
\end{gathered}
```

Inserting the expression for the fluxes into equation (2) we obtain:
```math
V \theta \frac{\partial \tilde{c}_i^j}{\partial t} + A \left(q \tilde{c}_{i}^j - \theta D_j \frac{\tilde{c}_{i+1}^j - \tilde{c}_i^j}{\Delta x} - q \tilde{c}_{i - 1}^j + \theta D_j \frac{\tilde{c}_i^j - \tilde{c}_{i-1}^j}{\Delta x}\right) = V \theta \tilde{r}_i^j
```

Dividing by $`V`$ and $`\theta`$ yields:
```math
\frac{\partial \tilde{c}_i^j}{\partial t} \frac{1}{\Delta x} \left(v \tilde{c}_{i}^j - D_j \frac{\tilde{c}_{i+1}^j - \tilde{c}_i^j}{\Delta x} - v \tilde{c}_{i - 1}^j + D_j \frac{\tilde{c}_i^j - \tilde{c}_{i-1}^j}{\Delta x}\right) = \tilde{r}_i^j
```

We can reformulate this equation and obtain a system of coupled ordinary differential equations:
```math
\frac{\partial \tilde{c}_i^j}{\partial t} = \tilde{r}_i^j + v \frac{\tilde{c}_{i - 1}^j - \tilde{c}_{i}^j }{\Delta x} + D_j \frac{\tilde{c}_{i+1}^j - 2 \tilde{c}_i^j + \tilde{c}_{i-1}^j}{(\Delta x)^2}
```

## Boundary Conditions
### Inflow Boundary
At the inflow boundaries, a fixed inflow concentration $`c_{\text{in}}^j(t)`$ is imposed.
For the dispersive flux we approximate the gradient using the inflow concentration and half the grid spacing:
```math
J_{1/2}^{j,\text{dis}} = - \theta D_j 2 \frac{\tilde{c}_1^j - c_{\text{in}}^j}{\Delta x}
```

For the advective flux we can directly use the inflow concentration:
```math
J_{1/2}^{j,\text{adv}} = q c_{\text{in}}^j
```
The overall equation for the boundary cells then becomes:
```math
\frac{\partial \tilde{c}_1^j}{\partial t} = \tilde{r}_1^j + v \frac{c_{\text{in}}^j - \tilde{c}_{1}^j }{\Delta x} + D_j \frac{\tilde{c}_{2}^j - 3 \tilde{c}_1^j + 2 c_{\text{in}}^j}{(\Delta x)^2}
```
 
### Outflow Boundary
At the outflow boundary, we assume a zero dispersive flux out of the domain.
The advective flux out of the domain can be computed using upstream weighting (as for all cells).
The overall equation for the outflow boundary cell is:
```math
\frac{\partial \tilde{c}_N^j}{\partial t} = \tilde{r}_N^j + v \frac{\tilde{c}_{N - 1}^j - \tilde{c}_{N}^j }{\Delta x} + D_j \frac{\tilde{c}_{N-1}^j - \tilde{c}_N^j}{(\Delta x)^2}
```

## Scaling of concentrations with for numeric stability
To improve numeric stability, state varibales can be scaled to the same order of magnitude using the parameter `c_norm`.
As we will show, this normalization constant cancels out in the transport terms and therefore is only required in teh reaction terms and boundary conditions.
This is the case because transport is linear and the term for one state contains only that state, not other states.

The system of ODEs for transport and reactions of species $j$ can be written as
```math
\frac{d\mathbf{c}_j}{dt} = \mathbf{M}_j \mathbf{c}_j + \mathbf{r}_j + \mathbf{b}_j
```
where $`\mathbf{M}_j`$ is the mobility matrix, $`\mathbf{r}_j`$ is the vector of reaction rates and $`\mathbf{b}_j`$ contains the boundary conditions.
Normalized cocentrations $`\mathbf{s}_j`$ are defined as
```math
\mathbf{s}_j = \frac{\mathbf{c}_j}{C_j}
```
with scaling constant $C_j$.
Thus, the ODE system for scaled concentrations is:
```math
\begin{aligned}
\frac{d\mathbf{s}_j}{dt} &= \frac{1}{C_j} \mathbf{M}_j \mathbf{c}_j + \frac{1}{C_j} \mathbf{r}_j + \frac{1}{C_j} \mathbf{b}_j\\
& = \frac{1}{C_j} \mathbf{M}_j \mathbf{s}_j C_j + \frac{1}{C_j} \mathbf{r}_j+ \frac{1}{C_j} \mathbf{b}_j\\
& = \mathbf{M} \mathbf{s}_j + \frac{1}{C_j} (\mathbf{r}_j+ \mathbf{b}_j)
\end{aligned}
```
We can see that the scaling constant cancels out in the transport part, but need to be considered in the reaction part and boundary conditions.

## References
Scheidegger, A. E. (1974). *The physics of flow through porous media* (3rd ed.). University of Toronto Press. https://doi.org/10.3138/9781487583750
