#!/bin/bash

# This requires to have the filter gitlab-math.lua installed (at ~/.pandoc/filters/)
# See also here: https://gist.github.com/MyriaCore/75729707404cba1c0de89cc03b7a6adf
# The filter itself can be found here: https://gist.github.com/lierdakil/00d8143465a488e0b854a3b4bf355cf6
pandoc --from markdown --to latex --pdf-engine=lualatex --self-contained --standalone --lua-filter gitlab-math.lua --katex --variable documentclass=scrartcl --variable fontfamily=libertinus -o theory_guide.pdf theory_guide.md
